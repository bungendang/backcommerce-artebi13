<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInformationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('store_informations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',25);
			$table->string('ownername',30);
			$table->string('address',50);
			$table->string('email',25);
			$table->string('phone',14);
			$table->string('logo');
			//$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store_informations');
	}

}
