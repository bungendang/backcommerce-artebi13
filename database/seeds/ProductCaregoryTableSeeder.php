<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Artebi\ProductCategory;

class ProductCategoryTableSeeder extends Seeder{
	public function run(){
		ProductCategory::create(array(
			'name' => 'T-shirt',
			'slug' => 'tshirt',
		));
		ProductCategory::create(array(
			'name' => 'Pants',
			'slug' => 'pants',
		));
	}	
}