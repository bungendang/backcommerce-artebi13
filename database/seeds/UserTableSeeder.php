<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Artebi\User;

class UserTableSeeder extends Seeder{
	public function run(){
		DB::table('users')->truncate();
		User::create(array(
			'username' => 'admin',
			'email' => 'admin@artebi13.com',
			'password' => Hash::make('rahasiah'),
			'firstname' => 'artebi',
			'lastname' => 'administrator',
			'phone'=> '+6281111111111',
			'status' => true,
			//'role_id'=>'66'
		));
	}	
}