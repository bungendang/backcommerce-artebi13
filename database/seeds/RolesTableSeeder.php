<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Artebi\Role;

class RolesTableSeeder extends Seeder{
	public function run(){
		Role::create(array(
			'id' => '66',
			'role_name' => 'admin'
		));
		Role::create(array(
			'id' => '77',
			'role_name' => 'kasir'
		));
		Role::create(array(
			'id' => '88',
			'role_name' => 'owner'
		));
	}	
}