<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Artebi\ProductCategory;

class CategoryTableSeeder extends Seeder{
	public function run(){
		ProductCategory::create(array(
			'name' => 'admin',
			'slug' => 'admin@artebi13.com',
		));
	}	
}