<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Artebi\ProductSize;

class ProductSizeTableSeeder extends Seeder{
	public function run(){
		ProductSize::create(array(
			'name' => 'Extra Large',
			'slug' => 'XL',
		));
		ProductSize::create(array(
			'name' => 'Large',
			'slug' => 'L',
		));
		ProductSize::create(array(
			'name' => 'Medium',
			'slug' => 'M',
		));
		ProductSize::create(array(
			'name' => 'Small',
			'slug' => 'S',
		));
	}	
}