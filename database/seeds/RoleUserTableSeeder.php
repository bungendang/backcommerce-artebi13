<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Artebi\RoleUser;

class RoleUserTableSeeder extends Seeder{
	public function run(){
		RoleUser::create(array(
			'user_id' => 1,
			'role_id' => 66,
		));
	}	
}