<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');
		$this->call('SizeTableSeeder');
		$this->call('RoleUserTableSeeder');
		$this->call('RolesTableSeeder');
		$this->call('ProductCategoryTableSeeder');

        $this->command->info('User table seeded!');
	}

}
