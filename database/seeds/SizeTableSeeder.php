<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Artebi\ProductSize;

class SizeTableSeeder extends Seeder{
	public function run(){
		ProductSize::create(array(
			'name' => 'large',
			'slug' => 'l',
		));
		ProductSize::create(array(
			'name' => 'medium',
			'slug' => 'm',
		));
		ProductSize::create(array(
			'name' => 'small',
			'slug' => 's',
		));
		ProductSize::create(array(
			'name' => 'All Size',
			'slug' => 'all',
		));
	}	
}