<?php namespace Artebi\Http\Controllers;

use Artebi\Http\Requests;
use Artebi\Http\Controllers\Controller;

use Artebi\StoreInformation;
use Request;
use Image;
use Hash;
use Artebi\RoleUser;
use Session;

use Artebi\User;
use Artebi\Product;
use Artebi\ProductSize;
use Artebi\ProductStock;
use Artebi\ProductImages;
use Artebi\Invoice;
use Artebi\DetailOrder;
use Artebi\Customer;
//use Illuminate\Http\Request;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$order = Invoice::orderBy('id', 'DESC')->get();
		$size = ProductSize::all();

		$products = Product::Join('product_categories', 'products.category_id', '=', 'product_categories.id')
            ->select('products.*', 'product_categories.name as cat_name')->get();

		return view('admin/index')->withOrderdata($order)->withData($products)->withSize($size);
	}

	/**
	 * Show the form for setting store information.
	 *
	 * @return Response
	 */
	public function getSettings()
	{
		$data = StoreInformation::find(1);
//		$data = $data[0];
		//return $data;
		return view('admin/settings')->withData($data);
	}

	public function postSettings()
	{
		$namatoko = Request::input('name');
		$namapemilik = Request::input('ownername');
		$alamat = Request::input('address');
		$email = Request::input('email');
		$telepon = Request::input('phone');
		$image = Request::file('image');

		if (is_null($image)) {
			# code...
			$imgurl = 'images/logos/logo-store.jpg';
		} else {
			# code...
			$destinationPath = 'images/logos/';
			$fileName = 'logo-store.jpg';
			$imgurl = $destinationPath.$fileName;

			$img = Image::make($image)->resize(null, 200, function ($constraint) {
    		$constraint->aspectRatio();
			});
			$img->save($imgurl);
		}

		$checking = StoreInformation::find(1);

		if (is_null($checking)) {
		    $data = new StoreInformation;
			$data->name = $namatoko;
			$data->ownername = $namapemilik;
			$data->address = $alamat;
			$data->email = $email;
			$data->phone = $telepon;
			$data->logo = $imgurl;
			$data->save();
		    //return 'siap';
		} else {
		    // It exists - remove from favorites button will show
		    $data = StoreInformation::find(1);
			$data->name = $namatoko;
			$data->ownername = $namapemilik;
			$data->address = $alamat;
			$data->email = $email;
			$data->phone = $telepon;
			$data->logo = $imgurl;
			$data->update();
		    //return 'adaan';
		}
		Session::flash('message', 'Informasi toko sudah diupdate!'); 
		Session::flash('alert-class', 'alert-success'); 

		return redirect('settings');
	}

	public function getProducts()
	{
		$products = Product::all();
		$size = ProductSize::all();

		$products = Product::Join('product_categories', 'products.category_id', '=', 'product_categories.id')
//            ->leftJoin('product_stocks','products.id','=','product_stocks.product_id')
//            ->Join('product_sizes','product_stocks.product_size_id','=','product_sizes.id')
            ->select('products.*', 'product_categories.name as cat_name')->get();
		
//		return $producthe;
            //$producthe = Product::find(28)->stocks()->get();
            //return $producthe;
		return view('admin/products')->withData($products)->withSize($size);
	}
	public function deleteProduct($id){
		$product = Product::find($id);
		$product->delete();
		Session::flash('message', 'Product deleted'); 
		Session::flash('alert-class', 'alert-success'); 
		return redirect('products');
	}
	public function editProduct($id){
		$product = Product::find($id);
		$product->name = Request::input('name');
		$product->description = Request::input('description');
		$product->price = Request::input('price');
		$product->save();
		$size = Request::input('size');
//		$cari = ProductStock::where('product_id','=',$product->id)->delete();

		if (empty($size)) {
			# code...
		} else {
			foreach ($size as $key => $value) {
			$stock = new ProductStock;
			$stock->product_id = $product->id;
			$stock->product_size_id = $value;
			$stock->save();
		}
		}

		
		Session::flash('message', 'Product updated'); 
		Session::flash('alert-class', 'alert-success'); 
		return redirect('product/'.$id);
	}
	public function getProductId($id)
	{
		$product = Product::find($id);
		//return $product;
		$size = ProductSize::all();
		$images = Product::find($id)->images()->get();
		$stocks = Product::find($id)->stocks()->get();

		return view('admin/product-detail')->withData($product)->withImages($images)->withStocks($stocks)->withSizes($size);
	}
	public function getOrders()
	{
		$order = Invoice::orderBy('id', 'DESC')->get();
		return view('admin/orders')->withOrderdata($order);
	}
	public function getUsers()
	{
		$userlist = User::Join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')
            ->select('users.*', 'roles.role_name')->get();
		//return $userlist;
		//$userlist = User::all();
		return view('admin/users')->withData($userlist);
	}
	public function postUserAdd()
	{
		$inputan = Request::all();
		$data = new User;
		$data->username = Request::input('username');
		$data->password = Hash::make(Request::input('password'));
		$data->email = Request::input('email');
		$data->firstname = Request::input('firstname');
		$data->lastname = Request::input('lastname');
		$data->phone = Request::input('phone');
		$data->save();
		//return $inputan;

		$role = new RoleUser;
		$role->user_id = $data->id;
		$role->role_id = Request::input('role');
		$role->save();


		Session::flash('message', 'User baru berhasil ditambahkan'); 
		Session::flash('alert-class', 'alert-success'); 
		return redirect('/users');
	}
	public function postProductAdd()
	{
		$inputan = Request::all();
		//return $inputan;
		$image = Request::file('image');

		$data = new Product;
		$data->category_id = Request::input('category');
		$data->name = Request::input('name');
		$data->price = Request::input('price');
		$data->description = Request::input('description');
		$data->save();


		$size = $inputan['size'];
		foreach ($size as $key => $value) {
			$stock = new ProductStock;
			$stock->product_id = $data->id;
			$stock->product_size_id = $value;
			$stock->save();
		}


		//return url();

		$img = new ProductImages;
		$img->product_id = $data->id;
		$img->primary_image = true;
		$img->url = url().'/images/products/'.$data->name.'.jpg'; 
		$img->save();

		$imgs = Image::make($image);
		$imgs->save('images/products/'.$data->name.'.jpg');
//		return $img->response('jpg');


		Session::flash('message', 'Barang sudah ditambahkan'); 
		Session::flash('alert-class', 'alert-success'); 

		return redirect('products');
	}

	public function postUpdateStock($id,$stockid){
		$input = Request::all();
		$data = ProductStock::find($stockid);

		$data->stock = $input['newstock'];

		$data->save();
		Session::flash('message', 'Stock barang berhasil diupdate'); 
		Session::flash('alert-class', 'alert-success'); 
		return redirect('product/'.$id);
	}
	public function getInvoiceNum($invnum){
		//return $invnum;
		$data = Invoice::where('invoice_num','=',$invnum)->first();
		$cust = Customer::find($data->customer_id);
		$detail = DetailOrder::where('invoice_id', '=', $data->id)
		->join('products', 'products.id', '=', 'detail_orders.product_id')->select('detail_orders.*','products.name as prodname','products.price as prodprice')->get();
				$order = Invoice::orderBy('id', 'DESC')->get();
		return view('admin/invoices')->withOrderdata($order)->withData($data)->withDetail($detail)->withCust($cust);
	}
	public function editStatus($id){
		$inv = Invoice::find($id);
		$inv->status = 1;
		$inv->save();
		return redirect('orders');
	}
	public function editCancel($id){
		$inv = Invoice::find($id);
		$inv->status = 0;
		$inv->save();
		return redirect('orders');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function uploadTest()
	{

	}

}
