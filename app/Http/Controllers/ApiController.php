<?php namespace Artebi\Http\Controllers;

use Artebi\Http\Requests;
use Artebi\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Artebi\StoreInformation;
use Artebi\Product;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function getInfo()
	{
		 $data = StoreInformation::find(1)->get();
        return response()->json(['informations'=>$data,'status'=>200]);
	}
	public function getAllUsers()
	{
		$userlist = User::Join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')
            ->select('users.*', 'roles.role_name')->get();
		return response()->json($userlist);
	}

	public function getAllProducts()
	{
		$products = Product::Join('product_images', 'products.id', '=', 'product_images.product_id')->select('products.*','product_images.url')->get();


		return response()->json(['products'=>$products]);
	}
	public function getAuthenticatedUser(Request $request)
	{
// grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
	}
	public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }
}
