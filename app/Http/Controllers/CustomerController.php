<?php namespace Artebi\Http\Controllers;

use Artebi\Http\Requests;
use Artebi\Http\Controllers\Controller;

use Auth;
use Hash;
use Session;
//use Illuminate\Http\Request;

use Request;
use Redirect;
use Artebi\Customer;
use Artebi\Product;
use Artebi\Invoice;
use Artebi\DetailOrder;
use Artebi\StoreInformation;
use Cart;

class CustomerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userdata = Customer::find(Auth::client()->id());
		return view('index')->withUserdata($userdata);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		return view('login');
	}
	public function postLogin(Request $request)
	{
		$usermail = Request::input('usermail');
		$password = Request::input('password'); 


    	if (Auth::client()->attempt(array(
        'username'     => $usermail,
        'password'  => $password,
    	))) {
    		return redirect('/')->withUserid(Auth::client()->check());  
    	} else {
    		return redirect('login');  
    	}
    	
//		return 'autentikasi';
	}
	public function getLogout(){
		Auth::client()->logout();
		return redirect('/');
	}
	public function getRegister()
	{
		return view('register');
	}
	public function postRegister()
	{
		$inputan = Request::all();
//		return $inputan;
		$data = new Customer;
		$data->username = Request::input('username');
		$data->password = Hash::make(Request::input('password'));
		$data->email = Request::input('email');
		$data->firstname = Request::input('firstname');
		$data->lastname = Request::input('lastname');
		$data->phone = Request::input('phone');
		$data->save();

		Session::flash('message', 'User baru berhasil ditambahkan'); 
		Session::flash('alert-class', 'alert-success'); 
		return redirect('login');
	}
	public function getProduct()
	{
		$userdata = Customer::find(Auth::client()->id());
		$product = Product::all();
		$products = Product::Join('product_images', 'products.id', '=', 'product_images.product_id')
//            ->leftJoin('product_stocks','products.id','=','product_stocks.product_id')
//            ->Join('product_sizes','product_stocks.product_size_id','=','product_sizes.id')
            ->select('products.*', 'product_images.url as images')->get();
		return view('product')->withProducts($products)->withUserdata($userdata);
	}
	public function getProductTshirt()
	{
		$userdata = Customer::find(Auth::client()->id());
		$product = Product::all();
		$products = Product::where('category_id',1)->Join('product_images', 'products.id', '=', 'product_images.product_id')
//            ->leftJoin('product_stocks','products.id','=','product_stocks.product_id')
//            ->Join('product_sizes','product_stocks.product_size_id','=','product_sizes.id')
            ->select('products.*', 'product_images.url as images')->get();
		return view('product')->withProducts($products)->withUserdata($userdata);
	}
	public function getProductDenim()
	{
		$userdata = Customer::find(Auth::client()->id());
		$product = Product::all();
		$products = Product::where('category_id',2)->Join('product_images', 'products.id', '=', 'product_images.product_id')
//            ->leftJoin('product_stocks','products.id','=','product_stocks.product_id')
//            ->Join('product_sizes','product_stocks.product_size_id','=','product_sizes.id')
            ->select('products.*', 'product_images.url as images')->get();
		return view('product')->withProducts($products)->withUserdata($userdata);
	}
	public function addToCart(Request $request)
	{

		$input = Request::all();
		Cart::add(Request::input('id'), Request::input('name'), Request::input('qty'), Request::input('price'), array('size' => Request::input('size')));
		Session::flash('message', 'Order ditambahkan'); 
		Session::flash('alert-class', 'alert-success'); 
		return redirect::back();
	}
	public function getCart()
	{
		$cart = Cart::content();
		//return $cart;
		return view('cart');
	}
	public function postCart()
	{
		if (Auth::client()->check())
		{
		    		$userdata = Customer::find(Auth::client()->id());
		//return $userdata->id;
		$content = Cart::content();
		$invoice = new Invoice;
		$invoice->invoice_num = rand(40000, 99999);
		$invoice->customer_id = $userdata->id;
		$invoice->total_price = Cart::total();
		$invoice->status = 'pending';
		$invoice->shipping_address = $userdata->address;
		$invoice->save();
		foreach ($content as $key) {
			//echo $key;
			$order = new DetailOrder;
			$order->product_id = $key->id;
			$order->invoice_id = $invoice->id;
			$order->qty = $key->qty;
			$order->save();
		}
		Cart::destroy();
		return redirect('product');
		}
		else{
		return redirect('login');			
		}


	}
	public function getAboutUs(){
		$userdata = Customer::find(Auth::client()->id());
		return view('about-us')->withUserdata($userdata);
	}
	public function getContactUs(){
		$userdata = Customer::find(Auth::client()->id());
		$storedata = StoreInformation::find(1);
//		return$storedata;
		return view('contact-us')->withUserdata($userdata)->withStoredata($storedata);
	}
	public function getProfile()
	{
		$userdata = Customer::find(Auth::client()->id());
		return view('profile')->withUserdata($userdata);
	}
	public function postProfileUpdate()
	{
		$input = Request::all();
		//return $input;
		$user = Customer::find(Auth::client()->id());
		$user->firstname = Request::input('firstname');
		$user->lastname = Request::input('lastname');
		$user->email = Request::input('email');
		$user->username = Request::input('username');
		$user->phone = Request::input('phone');
		$user->address = Request::input('address');
		$user->save();
		return redirect('profile');
	}
}
