<?php namespace Artebi\Http\Controllers;

use Artebi\Http\Requests;
use Artebi\Http\Controllers\Controller;
use Auth;

use Request;
use Artebi\User;
//use Illuminate\Http\Request;

class AuthController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for login admin.
	 *
	 * @return Response
	 */
	public function getAdminLogin()
	{
		return view('admin/login');
	}

	/**
	 * Process login for admin.
	 *
	 * @return Response
	 */
	public function postAdminLogin(Request $request)
	{
		$usermail = Request::input('usermail');
		$password = Request::input('password'); 


    	if (Auth::admin()->attempt(array(
        'username'     => $usermail,
        'password'  => $password,
    	))) {
    		return redirect('/');  
    	} else {
    		return redirect('login');  
    	}
    	
//		return 'autentikasi';
	}

	/**
	 * Process logout for admin.
	 *
	 * @return Response
	 */
	public function postLogout()
	{
		Auth::admin()->logout();
		return redirect('/');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
