<?php

use Artebi\StoreInformation;
use Artebi\Product;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::group(['domain' => 'admin.artebibackend.dev'], function($id)
{

    Route::get('/', 'AdminController@index');
    Route::get('login','AuthController@getAdminLogin');
    Route::post('login','AuthController@postAdminLogin');
    Route::get('/logout','AuthController@postLogout');

    Route::get('settings','AdminController@getSettings');
    Route::post('settings','AdminController@postSettings');

    Route::get('products','AdminController@getProducts');
    Route::get('product/{id}','AdminController@getProductId');
    Route::post('product/{id}/updatestock/{stockid}','AdminController@postUpdateStock');

    Route::post('product/add','AdminController@postProductAdd');
    Route::get('product/delete/{id}','AdminController@deleteProduct');
    Route::post('product/edit/{id}','AdminController@editProduct');
    Route::get('users','AdminController@getUsers');
    Route::get('user/add','AdminController@getUserAdd');
    Route::post('user/add','AdminController@postUserAdd');
    Route::get('orders','AdminController@getOrders');
    Route::get('invoice/{invnum}','AdminController@getInvoiceNum');
    Route::get('status/success/{id}','AdminController@editStatus');
    Route::get('status/cancel/{id}','AdminController@editCancel');

});


Route::get('/', 'CustomerController@index');
Route::get('login','CustomerController@getLogin');
Route::post('login','CustomerController@postLogin');
Route::get('logout','CustomerController@getLogout');
Route::get('register','CustomerController@getRegister');
Route::post('register','CustomerController@postRegister');
Route::get('product', 'CustomerController@getProduct');
Route::get('product/tshirt', 'CustomerController@getProductTshirt');
Route::get('product/denim', 'CustomerController@getProductDenim');
Route::post('addtocart','CustomerController@addToCart');
Route::get('cart','CustomerController@getCart');
Route::post('cart','CustomerController@postCart');
Route::get('profile','CustomerController@getProfile');
Route::post('profile/update','CustomerController@postProfileUpdate');
Route::get('about-us','CustomerController@getAboutUs');
Route::get('contact-us','CustomerController@getContactUs');
Route::group(['prefix' => 'api/v1','before' => 'jwt-auth'], function()
{
    Route::get('information','ApiController@getInfo');
    Route::get('products','ApiController@getAllProducts');
    Route::get('storeinmfos',function()
    {
        $data = StoreInformation::find(1)->get();
        return response()->json($data);

    });
    Route::post('jwt-login','ApiController@getAuthenticatedUser');
});

/*
Route::group(['prefix' => 'admin'], function()
{
    Route::get('/', ['middleware' => 'auth', function()
	{
    	// Only authenticated users may enter...
    	return view('admin/index');
	}]);
    Route::get('login','AuthController@getAdminLogin');
    Route::post('login','AuthController@postAdminLogin');
    Route::get('logout','AuthController@postLogout');

    Route::get('settings','AdminController@getSettings');
    Route::post('settings','AdminController@postSettings');

    Route::get('products','AdminController@getProducts');
    Route::get('users','AdminController@getUsers');
    Route::get('user/add','AdminController@getUserAdd');
    Route::post('user/add','AdminController@postUserAdd');
    Route::get('orders','AdminController@getOrders');

});*/

