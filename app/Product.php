<?php namespace Artebi;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	protected $hidden = ['created_at','updated_at'];

	public function stocks()
    {
        return $this->hasMany('Artebi\ProductStock','product_id');
    }
    public function images()
    {
        return $this->hasOne('Artebi\ProductImages','product_id');
    }
}
