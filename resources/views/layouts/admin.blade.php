<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTEBI13 | @yield('title')</title>
	<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
	@section('sidebar')
    <div class="col-sm-3 sidebar">
      	<ul class="list-group">
      		<li class="list-group-item"><a href="/"><i class="fa fa-tachometer"></i>&nbsp; dashboard</a></li>
      		<li class="list-group-item"><a href="/orders"><i class="fa fa-shopping-cart"></i>&nbsp; orders</a></li>
      		<li class="list-group-item"><a href="/products"><i class="fa fa-puzzle-piece"></i>&nbsp; products</a></li>
          <li class="list-group-item"><a href="/users"><i class="fa fa-user"></i>&nbsp; users</a></li>
      		<li class="list-group-item"><a href="/settings"><i class="fa fa-cog"></i>&nbsp; settings</a></li>
      	</ul>
    </div>
    @show
    <div class="col-sm-9 dashboard-content">
        @yield('content')
    </div>
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>