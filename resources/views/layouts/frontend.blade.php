<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTEBI13 | @yield('title')</title>
	<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">ARTEBI13</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="ae"><a href="/">HOME</a></li>
        <li><a href="/product">PRODUCT</a></li>
        <li><a href="/about-us">ABOUT</a></li>
        <li><a href="/contact-us">CONTACT</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/cart">{{ Cart::count()}} <i class="fa fa-shopping-cart"></i></a></li>
        @if (Auth::client()->check())
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{$userdata->username or 'Default' }} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/cart">Cart</a></li>
            <li><a href="/profile">Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/logout">Logout</a></li>
          </ul>
        </li>
        @else
          <li><a href="/login">LOGIN</a></li>
          <li><a href="/register">REGISTER</a></li>
        @endif
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
  <div class="main-content">
        @yield('content')
  </div>

  <div class="footer">
    <div class="container">
      <div class="address-bar text-right">
        Alamat: <br>
        Jl. Cemara Blok F No.36  <br>
        <i class="fa fa-phone"></i> 0821 182 686 44 <br>
        <i class="fa fa-minus-square"></i> artebiofficial <br>
        <i class="fa fa-envelope"></i> artebithirteen@gmail.com
      </div>
      <div class="copyright text-left">
        <i class="fa fa-copyright"></i> 2015 artebithirteen | All Right Reserved
      </div>
    </div>
  </div>
    
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>