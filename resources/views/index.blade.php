@extends('layouts.frontend')

@section('title', 'HOME')


@section('content')
<div class="home-img">
	<img src="/images/home-img-1.jpg" alt="" class="fullwidth">
</div>	

    <div class="container prod-img-link">
    	<div class="row">
    		<div class="col-md-6 text-center"><div class="text-center"><a href="/product/tshirt"><img src="/images/home-baju.jpg" alt=""></a></div></div>
    		<div class="col-md-6 text-center"><a href="/product/denim"><img src="/images/home-celana.jpg" alt=""></a></div>
    	</div>
    </div>
@stop