@extends('layouts.frontend')

@section('title', 'ABOUT US')

@section('sidebar')

@section('content')

<div class="image-about-us">
	<img src="/images/about-img-cropped.jpg" alt="" class="fullwidth">
</div>
<div class="about-us-content">
    <div class="container">
    <div class="row">
    	

    <div class="col-md-offset-1 col-md-10">
    	<h1>About Us</h1>
    	<p>ARTEBITHIRTEEN adalah outlet penjualan baju berupa T-Shirt bagi kaum adam dengan design yang cocok untuk anak muda usia 15 hingga 23 tahun yang bergerak secara online memanfaatkan media social. Toko Artebithirteen clothing pertama kali berdiri di Bandung, bertempat di jalan Hasanudin no.26 (dipatiukur) Bandung, pada tahun 2010. Berawal dari Agung subarya yang mempunyai ide membuat suatu usaha secara online yang bergerak dalam bidang fashion yang akhir-akhir ini banyak digandrungi oleh para anak muda yang menyukai fashion, dalam membuat Artebithirteen ini agung dibantu oleh saudara dan temanya guna untuk menanam modal ataupun memberikan ide-ide maupun gagasan-gagasan dalam membuat Artebithirteen ini agar dapat ramai di disukai oleh para anak muda dan dapat menjadikan bisnis ini banyak digemari berbagai pihak, baik sekedar pengisi waktu luang, mendapatkan tambahan pendapatan, maupun ditekuni sebagai mata pencaharian utama.</p>
    	<p>Artebithirten sendiri dalam proses pembuatan designya mereka membuatnya dengan sendiri memanfaatkan kemampuan salah satu owners dari Artebithirteen dalam mendesign sebuah gambar hingga sampai dengan terwujudunya suatu design, kemudian merealisasikan design ke T-shirt dan dijual melalui online. Dalam hal design ini Artebithirteen mengutamakan design yang menarik dan merupakan Limited Edition Product yang biasanya akan diburu konsumen, karena lebih dirasa unik dan private. Karena terkendala biaya untuk menyewa atau membeli tempat, sebagian besar penjualan di lakukan secara online dengan memanfaatkan media social, maka dari itu Artebithirteen belum membuka toko resmi.</p>
    	<p>Seiring berjalannya waktu dan bertambahnya konsumen yang menyukai produk-produk dari Toko Artebithirteen Clothing ini membuat para owners Artebithirteen memberanikan diri untuk membuat berbagai macam produk yang akan mereka jual, tidak hanya t-shirt saja sekarang yang dijual ditoko ini adapun berbagai macam jenis yang bertambah seperti celana, jaket, topi, dan lain-lain.</p>
    </div>
    </div>    	
    </div>
</div>
@stop