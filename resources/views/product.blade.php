@extends('layouts.frontend')

@section('title', 'PRODUCT')

@section('content')
    <div class="container">
       @if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }} fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
@endif
<div class="product-body">
        <div class="prod-cat-select">
            <ul>
                <li><a href="/product/">All</a></li>
                <li>/</li>
                <li><a href="/product/tshirt">T-shirt</a></li>
                <li>/</li>
                <li><a href="/product/denim">Denim</a></li>
            </ul>
        </div>
        <div class="row">
    	@foreach ($products as $product)
    	<div class="col-sm-4">
    	<div class="panel panel-default">
	    	<div class="panel-heading">{{$product->name}}</div>
		  <div class="panel-body">
		    <div class="thumb">
    			<img src="{{$product->images}}" alt="" class="img-responsive">
    			<form action="/addtocart" method="POST">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    				<input type="hidden" name="id" value="{{$product->id}}">
                    <input type="hidden" name="name" value="{{$product->name}}">
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <label for="size">Size</label>
    				<select name="size" id="size">
                              <?php 
            $stocks = DB::table('product_stocks')
            ->where('product_id' , $product->id)
            ->join('product_sizes', 'product_stocks.product_size_id', '=', 'product_sizes.id')
            ->select('product_stocks.stock','product_sizes.slug','product_sizes.id as prodid')
            ->get();

           ?>            @foreach ($stocks as $stock)
            <option value="{{$stock->prodid}}">{{strtoupper($stock->slug)}} - sisa stock {{$stock->stock}}</option> 
            @endforeach
    					
    				</select>
                    <label for="qty">Qty</label>
                    <select name="qty" id="qty">
                        @for ($i = 1; $i < 24; $i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                    <button class="btn btn-primary btn-xs"><i class="fa fa-shopping-cart"></i></button>
    			</form>
    			
    		</div>
		  </div>
		</div>
    		
    	</div>
		@endforeach
        </div>
    </div>
    </div>
@stop