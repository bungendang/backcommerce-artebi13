@extends('layouts.admin')

@section('title', 'DASHBOARD')

@section('sidebar')

@section('content')
 <div class="row">
   <div class="col-sm-4">
    <div class="product-images thumbnail">
     <img src="{{$images[0]->url}}" alt="" class="img-rounded">
    </div>     
   </div>
   <div class="col-sm-8">
   @if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }} fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
@endif
      <table class="table">
        <tr>
          <td>Nama</td>
          <td>{{$data->name}}</td>
        </tr>
        <tr>
          <td>Harga</td>
          <td>Rp.{{$data->price}}</td>
        </tr>
        <tr>
          <td>Deskripsi Produk</td>
          <td>{{$data->description}}</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Stock</strong></td>
        </tr>
        @foreach($stocks as $stock)
        <?php 
          $size = DB::table('product_sizes')->where('id', $stock->product_size_id)->first();
          //var_dump($user)
         ?>
         <tr>
          
           <td>{{strtoupper($size->name)}}</td>
           <td>
           <div>
             <form action="/product/{{$stock->product_id}}/updatestock/{{$stock->id}}" method="POST">
             <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="input-group input-group-sm">
      <input type="text" name="newstock" class="form-control" placeholder="" value="{{$stock->stock}} ">
      <span class="input-group-btn btn-group-sm">
        <button class="btn btn-default" type="submit">Update</button>
      </span>
    </div><!-- /input-group -->

              </form>
           </div></td>
              
         </tr>
        @endforeach
        
      </table>
         <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddProduct">
      Edit Product
    </button> 
   </div>
 </div>








<div class="modal fade" id="modalAddProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Edit Product</h4>
      </div>
      <form action="/product/edit/{{$data->id}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="modal-body">
      <div class="form-group">
          <label for="role" class="col-sm-4 control-label">Kategori</label>
            <div class="col-sm-4">
            <select name="category" id="category" class="form-control">
              <option value="1" <?php $data->category_id == 12 ? "selected":""?>>Baju </option>
              <option value="2" <?php $data->category_id == 1 ? "selected":""?>>Celana</option>
            </select>
          </div>
        </div>
          <div class="form-group">
            <label for="name" class="col-sm-4 control-label">Nama Barang</label>
            <div class="col-sm-8"><input type="text" class="form-control" id="name" name="name" value="{{$data->name}}"></div>
          </div>
          <div class="form-group">
            <label for="size" class="col-sm-4 control-label">Ukuran</label>
            <div class="col-sm-8">
            @foreach ($sizes as $s)
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="size[]" value="{{$s->id}}">
                  {{strtoupper($s->name)}}
                </label>
              </div>
            @endforeach
            </div>
          </div>
          <div class="form-group">
            <label for="price" class="col-sm-4 control-label">Harga</label>
            <div class="col-sm-5">
              <div class="input-group">
          <div class="input-group-addon">Rp.</div>
        <input type="text" class="form-control" id="price" name="price" value="{{$data->price}}">
          </div>
            </div>
          </div>
          <div class="form-group">
            <label for="description" class="col-sm-4 control-label">Deskripsi Barang</label>
            <div class="col-sm-8">
            <textarea name="description" id="description" class="form-control" cols="30" rows="10">{{$data->description}}</textarea>
            </div>
          </div>
       </div>
      <div class="modal-footer">
        <!--button type="reset" class="btn btn-warning">Reset</button-->
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>



@stop
