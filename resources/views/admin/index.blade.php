@extends('layouts.admin')

@section('title', 'DASHBOARD')

@section('sidebar')

@section('content')


    <div class="row">
    	<div class="col-md-6">
    		<table class="table table-bordered">
				<tr>
					<th>No</th>
					<th>Nama Customer</th>
					<th>Invoice Number</th>
					<th>Status</th>
				</tr>
        <?php $i=1 ?>
        @foreach ($orderdata as $ord)
        <?php if ($ord->status == 0): $status = 'unpaid'?>
          <tr class="warning">
        <?php elseif($ord->status == 1): $status = 'paid'?>
        <tr class="success">
        <?php endif ?>




              <td>{{$i++}}</td>
              <?php $customer = DB::table('customers')->where('id','=', $ord->customer_id)->first();
              //return $customer
              ?>

              <td>{{$customer->firstname}} {{$customer->lastname}}</td>
              <td><a href="/invoice/{{$ord->invoice_num}}">{{$ord->invoice_num}}</a></td>
              <td>{{$status}} <span><a href="#"></a></span><div class="btn-group  pull-right">
  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-pencil-square-o"></i> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li>
      <a href="/status/success/{{$ord->id}}">Success</a>
    </li>
    <li>
      <a href="/status/cancel/{{$ord->id}}">Cancel</a>
    </li>
  </ul>
</div></td>
            </tr> 
        @endforeach
			</table>
    	</div>
    	<div class="col-md-6">
<table class="table table-bordered">
    		<tr>
          <th>No</th>  
          <th>Nama</th>
          <th>Category</th>
          <th>Harga</th>
          <th>Size/Stock</th>
        </tr>
                <?php $no = 1 ?>
        @foreach ($data as $product)
        <tr>
          <td>{{$no++}}</td>
          <td><a href="/product/{{$product->id}}">{{$product->name}}</a></td>
          <td>{{$product->cat_name}}</td>
          <td>{{$product->price}}</td>
          <?php 
            $stocks = DB::table('product_stocks')
            ->where('product_id' , $product->id)
            ->join('product_sizes', 'product_stocks.product_size_id', '=', 'product_sizes.id')
            ->select('product_stocks.stock','product_sizes.slug')
            ->get();
           ?>

          <td>
            @foreach ($stocks as $stock)
            {{strtoupper($stock->slug)}}  ({{$stock->stock}})
            @endforeach
            <span class="pull-right"><a href="/product/delete/{{$product->id}}"><i class="fa fa-trash-o"></i></a></span>
          </td>
        </tr>    
        @endforeach
    	</table>
    	</div>
    </div>
@stop