@extends('layouts.admin')

@section('title', 'DASHBOARD')

@section('sidebar')

@section('content')

  <div class="invoice">
    <div class="cotainer">
      <div class="row">
        <div class="col-xs-6">
          <h1>
            <img src="/images/logos/logo-store.jpg" class="thumbnail-logos">
            Artebithirteen
          </h1>
        </div>
        <div class="col-xs-6 text-right">
          <h1>INVOICE</h1>
          <h1><small>Invoice #{{$data->invoice_num}}</small></h1>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5 col-xs-text-right">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>To : <a href="#">{{strtoupper($cust->firstname)}} {{strtoupper($cust->lastname)}}</a></h4>
            </div>
            <div class="panel-body">
              <p>
                {{$data->shipping_address}} <br>
                {{$cust->phone}} <br>
                {{$cust->email}}
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- / end client details section -->
      <table class="table table-bordered">
        <tr>
          <th>Produk</th>
          <th>Qty</th>
          <th>Harga</th>
          <th>Sub Total</th>
        </tr>
        <?php $total = 0; ?>
        @foreach($detail as $detor)
        <tr>
          <td>{{$detor->prodname}}</td>
          <td>{{$detor->qty}}</td>
          <td class="text-right">{{$detor->prodprice}}</td>
          <td class="text-right"><?php 
          
          $subtotal = $detor->qty * $detor->prodprice;
          $total = $total + $subtotal; ?>{{$subtotal}}</td>
        </tr>
        @endforeach
      </table>
      <div class="row text-right">
        <div class="col-xs-2 col-xs-offset-7">
          <p>
          <?php  ?>
            <strong>
            Total : <br>
            </strong>
          </p>
        </div>
        <div class="col-xs-3">
          <strong>
          Rp.{{$total}} <br>
          </strong>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h4>Bank details</h4>
            </div>
            <div class="panel-body">
              <p>Artebi13</p>
              <p>BCA</p>
              <p>No. Rekening : 2800895327</p>
            </div>
          </div>
        </div>
        <div class="col-xs-7">
          <!--div class="span7">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4>Contact Details</h4>
              </div>
              <div class="panel-body">
                <p>
                  Email : you@example.com <br><br>
                  Mobile : -------- <br> <br>
                  Twitter : <a href="https://twitter.com/tahirtaous">@TahirTaous</a>
                </p>
                <h4>Payment should be made by Bank Transfer</h4>
              </div>
            </div>
          </div-->
        </div>
      </div>
    </div>
  </div>
@stop
