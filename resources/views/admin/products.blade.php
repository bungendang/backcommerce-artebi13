@extends('layouts.admin')

@section('title', 'DASHBOARD')

@section('sidebar')

@section('content')
@if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }} fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
@endif
	<div class="action-row">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddProduct">
		  Add Product
		</button>
	</div>
    <div class="list-product">
    	<table class="table table-bordered">
    		<tr>
          <th>No</th>  
          <th>Nama</th>
          <th>Category</th>
          <th>Harga</th>
          <th>Size/Stock</th>
        </tr>
        <?php $no = 1 ?>
        @foreach ($data as $product)
        <tr>
          <td>{{$no++}}</td>
          <td><a href="/product/{{$product->id}}">{{$product->name}}</a></td>
          <td>{{$product->cat_name}}</td>
          <td>{{$product->price}}</td>
          <?php 
            $stocks = DB::table('product_stocks')
            ->where('product_id' , $product->id)
            ->join('product_sizes', 'product_stocks.product_size_id', '=', 'product_sizes.id')
            ->select('product_stocks.stock','product_sizes.slug')
            ->get();
           ?>

          <td>
            @foreach ($stocks as $stock)
            {{strtoupper($stock->slug)}}  ({{$stock->stock}})
            @endforeach
            <span class="pull-right"><a href="/product/delete/{{$product->id}}"><i class="fa fa-trash-o"></i></a></span>
          </td>
        </tr>    
        @endforeach
    	</table>
    </div>









    <!-- Modal -->
<div class="modal fade" id="modalAddProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Tambah Product</h4>
      </div>
      <form action="/product/add" method="POST" enctype="multipart/form-data" class="form-horizontal">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="modal-body">
     	<div class="form-group">
        	<label for="role" class="col-sm-4 control-label">Kategori</label>
          	<div class="col-sm-4">
            <select name="category" id="category" class="form-control">
              <option value="1">Baju</option>
              <option value="2">Celana</option>
            </select>
          </div>
        </div>
          <div class="form-group">
            <label for="name" class="col-sm-4 control-label">Nama Barang</label>
            <div class="col-sm-8"><input type="text" class="form-control" id="name" name="name"></div>
          </div>
          <div class="form-group">
            <label for="size" class="col-sm-4 control-label">Ukuran</label>
            <div class="col-sm-8">
            @foreach ($size as $s)
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="size[]" value="{{$s->id}}">
                  {{strtoupper($s->name)}}
                </label>
              </div>
            @endforeach
            </div>
          </div>
          <div class="form-group">
            <label for="price" class="col-sm-4 control-label">Harga</label>
            <div class="col-sm-5">
            	<div class="input-group">
			    <div class="input-group-addon">Rp.</div>
				<input type="text" class="form-control" id="price" name="price">
    			</div>
            </div>
          </div>
          <div class="form-group">
            <label for="description" class="col-sm-4 control-label">Deskripsi Barang</label>
            <div class="col-sm-8">
            <textarea name="description" id="description" class="form-control" cols="30" rows="10"></textarea>
            </div>
          </div>
          <div class="form-group">
          	<label for="foto" class="col-sm-4 control-label">Foto</label>
          	<div class="col-sm-8">
          		<input type="file" id="image" name="image">
          	</div>
          </div>
       </div>
      <div class="modal-footer">
        <!--button type="reset" class="btn btn-warning">Reset</button-->
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@stop
