<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTEBI13 | LOGIN ADMIN</title>
	<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
	  <link rel="stylesheet" href="/bower_components/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
<div class="container">
	<form class="form-signin" action="login" method="POST">
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" id="usermail" name="usermail" class="form-control" placeholder="username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
      </div>
</body>
</html>
