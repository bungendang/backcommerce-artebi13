@extends('layouts.admin')

@section('title', 'DASHBOARD')

@section('sidebar')

@section('content')
@if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }} fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
@endif
	<table class="table table-bordered">
		<tr>
			<th>No</th>
      <th>Username</th>
			<th>Nama</th>
			<th>Type</th>
		</tr>
    <?php $no = 1 ?>
		@foreach ($data as $user)
		<tr>
			<td>{{$no++}}</td>
      <td>{{$user->username}}</td>
			<td>{{$user->firstname}} {{$user->lastname}}</td>
			<td>{{$user->role_name}}</td>
		</tr>    
		@endforeach

	</table>
	<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddUser">
  Add User
</button>
<!-- Modal -->
<div class="modal fade" id="modalAddUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Tambah User</h4>
      </div>
      <form action="/user/add" method="POST" class="form-horizontal">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="modal-body">
          <div class="form-group">
            <label for="name" class="col-sm-4 control-label">Nama Lengkap</label>
            <div class="col-sm-4"><input type="text" class="form-control" id="firstname" name="firstname"></div>
            <div class="col-sm-4"><input type="text" class="form-control" id="lastname" name="lastname"></div>
          </div>
          <div class="form-group">
            <label for="username" class="col-sm-4 control-label">Username</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="username" name="username">
            </div>
          </div>
          <div class="form-group">
            <label for="password" class="col-sm-4 control-label">Password</label>
            <div class="col-sm-5"><input type="password" class="form-control" id="password" name="password"></div>
          </div>
          <div class="form-group">
            <label for="email" class="col-sm-4 control-label">E-mail</label>
            <div class="col-sm-5"><input type="text" class="form-control" id="email" name="email"></div>
          </div>
          <div class="form-group">
            <label for="phone" class="col-sm-4 control-label">Nomor Telepon</label>
            <div class="col-sm-5"><input type="text" class="form-control" id="phone" name="phone"></div>
          </div>
          <div class="form-group">
            <label for="role" class="col-sm-4 control-label">Role</label>
            <div class="col-sm-5">
              <select name="role" id="role" class="form-control">
                <option value="66">Admin</option>
                <option value="77">Kasir</option>
                <option value="88">Owner</option>
              </select>
            </div>
          </div>
        </div>
      <div class="modal-footer">
        <!--button type="reset" class="btn btn-warning">Reset</button-->
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@stop
