@extends('layouts.admin')

@section('title', 'SETTINGS')

@section('sidebar')

@section('content')
@if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }} fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
@endif

    
    <form action="settings" method="POST" class="form-horizontal" enctype="multipart/form-data">
    <div class="row">
    <div class="col-sm-8">
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Nama Toko</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="name" name="name" placeholder="{{ isset($data->name) ? $data->name : '' }}" value="{{ isset($data->name) ? $data->name : '' }}">                
                </div>
            </div>
            <div class="form-group">
                <label for="ownername" class="col-sm-3 control-label">Nama Pemilik</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="ownername" name="ownername" placeholder="{{ isset($data->ownername) ? $data->ownername : '' }}" value="{{ isset($data->ownername) ? $data->ownername : '' }}">
                </div>
            </div>
            <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Alamat Toko</label>
                <div class="col-sm-9">
                    <textarea name="address" id="address" class="form-control" cols="30" rows="5">{{ isset($data->address) ? $data->address : '' }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">E-mail</label>
                <div class="col-sm-9">
                    <input type="email" id="email" name="email" class="form-control" placeholder="{{ isset($data->email) ? $data->email : '' }}" value="{{ isset($data->email) ? $data->email : '' }}">
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Telepon</label>
                <div class="col-sm-9">
                    <input type="text" id="phone" name="phone" class="form-control" placeholder="{{ isset($data->phone) ? $data->phone : '' }}" value="{{ isset($data->phone) ? $data->phone : '' }}">
                </div>
            </div>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            
                
    </div>
    <div class="col-sm-2">
        <div class="logostore">
            <img src="/{{ isset($data->logo) ? $data->logo : '' }}" alt="">
        </div>
        <input type="file" id="image" name="image">
    </div>
    </div>
    <button class="btn btn-primary">SAVE</button>
    </form>
@stop