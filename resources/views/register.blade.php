@extends('layouts.frontend')

@section('title', 'HOME')


@section('content')
<div class="container">
    	<form class="form-register" action="register" method="POST">
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <h2 class="form-signin-heading">Register</h2>
        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" id="username" name="username" class="form-control" placeholder="username" required autofocus>
        <label for="email" class="sr-only">Email</label>
        <input type="text" id="email" name="email" class="form-control" placeholder="email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="password" required>
        <label for="inputFirstname" class="sr-only">Firstname</label>
        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="firstname" required>
        <label for="inputLastname" class="sr-only">Lastname</label>
        <input type="text" id="lastname" name="lastname" class="form-control" placeholder="lastname" required>
        <label for="inputPhone" class="sr-only">Phone</label>
        <input type="text" id="phone" name="phone" class="form-control" placeholder="phone" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
      </form>	
</div>
@stop