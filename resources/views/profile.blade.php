@extends('layouts.frontend')

@section('title', 'PROFILE')

@section('sidebar')

@section('content')
    <div class="container">
    	<h1>PROFILE</h1>
    	<table class="table table-bordered">
    		<tr>
    			<td>Nama</td>
    			<td>{{$userdata->firstname}} {{$userdata->lastname}}</td>
    		</tr>
    		<tr>
    			<td>No telepon</td>
    			<td>{{$userdata->phone}}</td>
    		</tr>
    		<tr>
    			<td>Username</td>
    			<td>{{$userdata->username}}</td>
    		</tr>
    		<tr>
    			<td>Email</td>
    			<td>{{$userdata->email}}</td>
    		</tr>
    		<tr>
    			<td>Alamat</td>
    			<td>{{$userdata->address}}</td>
    		</tr>
			<tr>
				<td class="text-right" colspan="2"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalUpdateProfile">
		  Update User
		</button></td>
			</tr>
    	</table>
    </div>



        <!-- Modal -->
<div class="modal fade" id="modalUpdateProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Profile</h4>
      </div>
      <form action="/profile/update" method="POST" enctype="multipart/form-data" class="form-horizontal">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="modal-body">
     	<div class="form-group">
        	<label for="firstname" class="col-sm-4 control-label">Nama Depan</label>
          	<div class="col-sm-6">
          	<input type="text" class="form-control" name="firstname" id="firstname" value="{{$userdata->firstname}}">
          </div>
          </div>
          <div class="form-group">
        	<label for="lastname" class="col-sm-4 control-label">Nama Belakang</label>
          	<div class="col-sm-6">
          	<input type="text" class="form-control" name="lastname" id="lastname" value="{{$userdata->lastname}}">
          </div>
        </div>
                  <div class="form-group">
        	<label for="email" class="col-sm-4 control-label">Email</label>
          	<div class="col-sm-6">
          	<input type="text" class="form-control" name="email" id="email" value="{{$userdata->email}}">
          </div>
        </div>
                          <div class="form-group">
        	<label for="phone" class="col-sm-4 control-label">No telepon</label>
          	<div class="col-sm-6">
          	<input type="text" class="form-control" name="phone" id="phone" value="{{$userdata->phone}}">
          </div>
        </div>
                  <div class="form-group">
        	<label for="username" class="col-sm-4 control-label">Username</label>
          	<div class="col-sm-6">
          	<input type="text" class="form-control" name="username" id="username" value="{{$userdata->username}}">
          </div>
        </div>
                  <div class="form-group">
        	<label for="address" class="col-sm-4 control-label">Address</label>
          	<div class="col-sm-6">
          	<textarea name="address" id="address" class="form-control">{{$userdata->address}}</textarea>
          	
          </div>
        </div>
       </div>
      <div class="modal-footer">
        <!--button type="reset" class="btn btn-warning">Reset</button-->
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@stop