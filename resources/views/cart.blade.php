@extends('layouts.frontend')

@section('title', 'CART')

@section('content')
<div class="container">
    	<?php $cartcont = Cart::content() ?>
        <form action="/cart" method="POST">
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

    	<table class="table table-bordered">
    		<tr>
    			<th>Nama Produk</th>
    			<th>Qty</th>
    			<th>Price</th>
    			<th>Subtotal</th>
    		</tr>
    		@foreach($cartcont as $content)
    		<tr>
				<td>{{$content->name}}</td>
				<td>{{$content->qty}}</td>
				<td>{{$content->price}}</td>
				<td>{{$content->subtotal}}</td>
    		</tr>
    		@endforeach
    		<tr>
    			<td colspan="3">Total</td>
    			<?php $total = Cart::total();
    			
    			?>
    			<td>{{$total}}</td>
    		</tr>
            <tr>
                <td colspan="4" class="text-right"><button type="submit" class="btn btn-primary">Submit</button></td>
            </tr>

    	</table>
        </form>
</div>

@stop