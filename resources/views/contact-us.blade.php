@extends('layouts.frontend')

@section('title', 'ABOUT US')

@section('sidebar')

@section('content')
<div class="contact-us">

    <div class="container">
        <div class="col-md-6 text-center">
            <img src="/{{$storedata->logo}}" alt="">

        </div>
        <div class="col-md-6">
        <h1>Contact Us</h1>
        <p>{{$storedata->address}}
        <br><strong>Telp:</strong> <br>
        {{$storedata->phone}}<br>
        <strong>LINE:</strong> <br>
        artebiofficial <br>
        <strong>EMAIL:</strong> <br>
        artebithirteen@gmail.com
        </p>                    

        
        </div>

    </div>
</div>
@stop